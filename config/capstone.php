<?php

return [
    'features' => [
        'redirectable-content' => env('LINTOL_FEATURE_REDIRECTABLE_CONTENT', false),
        'remote-data-resources' => env('LINTOL_FEATURE_REMOTE_DATA_RESOURCES', false),
        'services-github' => env('LINTOL_FEATURE_SERVICES_GITHUB', false),
        'export-csv' => env('LINTOL_FEATURE_EXPORT_CSV', false),

        /**
         * This should _never_ be on on a public-facing site - it is for local testing
         * only when an OAuth provider is not available, and is passwordless.
         *
         * It should be false, or the email address of the admin user to auto-login.
         */
        'local-admin-login' => env('LINTOL_FEATURE_LOCAL_ADMIN_LOGIN', false),
    ],
    'examples' => [
        'classify-category' => [
            'category-server-url' => env('LINTOL_EXAMPLE_DT_CLASSIFY_CATEGORY_SERVER_URL', 'http://localhost:5000/')
        ]
    ],
    'documents' => [
        'terms-and-conditions' => env('LINTOL_DOCUMENT_PREFIX', '') . '/saas-terms-support.pdf',
        'privacy-notice' => env('LINTOL_DOCUMENT_PREFIX', '') . '/privacy-terms.pdf'
    ],
    'wamp' => [
        'realm' => env('LINTOL_CAPSTONE_REALM', 'realm1'),
        'url' => env('LINTOL_CAPSTONE_URL', 'ws://172.26.0.1:8081/ws'),
        'doorstep-retry-delay' => env('LINTOL_CAPSTONE_DOORSTEP_RETRY_DELAY', 120),
    ],
    'frontend' => [
        'url' => env('LINTOL_FRONTEND_URL', '/'),
        'proxy' => env('LINTOL_FRONTEND_PROXY', false),
        'max-pagination' => env('LINTOL_MAX_PAGINATION', 200)
    ],
    'encryption' => [
        'blind-index-key' => env('LINTOL_BLIND_INDEX_KEY', config('app.key'))
    ],
    'http' => [
        'ckan' => [
            'request-options' => [
                'force_ip_resolve' => 'v4'
            ]
        ]
    ],
    'authentication' => [
        'ckan' => [
            'valid-servers' => env('LINTOL_CKAN_SERVERS', false) ? explode(';', env('LINTOL_CKAN_SERVERS')) : []
        ]
    ],
    'processors' => [
        'allowed-supplementary-domains' => explode(',', env('LINTOL_SUPPLEMENTARY_DOMAINS', '')),
        'timeout-search-since-days' => intval(env('LINTOL_TIMEOUT_SEARCH_DAYS_SINCE', 30000)),
        'timeout-default-seconds' => intval(env('LINTOL_TIMEOUT_DEFAULT_SECONDS', 3600)),
    ]
];
