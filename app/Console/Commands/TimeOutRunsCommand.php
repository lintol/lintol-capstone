<?php

namespace App\Console\Commands;

use Crypt;
use Queue;
use Carbon\Carbon;
use App\StatusTracking;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\Models\ValidationRun;
use Illuminate\Console\Command;

class TimeOutRunsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ltl:time-out-runs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if any runs should be marked failed having timed out';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $since = (new Carbon())->subDays(config('capstone.processors.timeout-search-since-days', 1));
        $unfinishedValidationRuns = ValidationRun::where('initiated_at', '>', $since)
            ->where(function ($q) {
                return $q->whereNull('completion_status')
                         ->orWhere('completion_status', 0)
                         ->orWhereNull('completed_at');
            })->get();

        // This is in PHP to allow for per-processor/profile timeouts
        $since = (new Carbon())->subSeconds(config('capstone.processors.timeout-default-seconds', 3600));
        $unfinishedValidationRuns->each(function ($run) use ($since) {
            if ($run->initiated_at < $since) {
                $run->markCompleted(false);
            }
        });

        $statusTracking = new StatusTracking;
        $dataResourceModel = app()->make(DataResource::class);
        $validationRunModel = app()->make(ValidationRun::class);

        $statusTracking->statuses = [
            'resource_statuses' => $dataResourceModel->summaryByStatus(),
            'run_statuses' => $validationRunModel->summaryByStatus(),
            'jobs' => Queue::size()
        ];
        $statusTracking->save();
    }
}
