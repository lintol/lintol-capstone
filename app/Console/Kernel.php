<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CreateCkanInstanceToken::class,
        Commands\RecordStatusesCommand::class,
        Commands\TimeOutRunsCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('ltl:status-retrieve')
            ->cron(config('additional.status-retrieve-frequency'));

        $schedule->command('ltl:check-scheduled')
            ->cron(config('additional.check-scheduled-frequency', '* * * * *'));

        $schedule->command('ltl:record-statuses')
            ->cron(config('additional.record-status-frequency'));

        $schedule->command('ltl:time-out-runs')
            ->cron('* * * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
