<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Route;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Lintol\Capstone\Models\DataResource::class => \App\Policies\DataResourcePolicy::class,
        \Lintol\Capstone\Models\Profile::class => \App\Policies\ProfilePolicy::class,
        \Lintol\Capstone\Models\Processor::class => \App\Policies\ProcessorPolicy::class,
        \Lintol\Capstone\Models\ValidationRun::class => \App\Policies\ValidationRunPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::loadKeysFrom(config('auth.keys.path', storage_path()));
        Passport::routes();

        // Middleware `api` that contains the `custom-provider` middleware group defined on $middlewareGroups above
        Route::group(['middleware' => 'api'], function () {
            Passport::routes(function ($router) {
                return $router->forAccessTokens();
            });
        });
    }
}
