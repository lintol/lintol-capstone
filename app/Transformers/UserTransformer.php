<?php

namespace App\Transformers;

use League\Fractal;
use App\User;

class UserTransformer extends Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
        // At present feature flags are for all users,
        // but this will allow per-user flags to be
        // communicated to the front-end
        return [
          'id' => $user->id,
          'name' => $user->name ? $user->name : $user->nickname,
          'nickname' => $user->nickname,
          'avatar' => $user->avatar,
          'email' => $user->email,
          'driver' => $user->driver,
          'driverServer' => $user->driver_server,
          'present' => $user->present,
          'featureFlags' => $user->featureFlags
        ];
    }
}
