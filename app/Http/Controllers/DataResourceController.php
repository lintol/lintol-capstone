<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Log;
use App\User;
use App\StatusTracking;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\Models\ValidationRun;
use Illuminate\Http\Request;
use Lintol\Capstone\Transformers\DataResourceTransformer;
use Lintol\Capstone\ResourceManager;

class DataResourceController extends Controller
{
    protected $validFilters = ['filetype', 'user_id', 'created_at', 'organization'];
    protected $validSortBy = ['filetype', 'packageName', 'created_at', 'status', 'user_id'];
    protected $sortByMappingRemote = ['packageName' => 'title', 'created_at' => 'created'];
    protected $sortByMappingLocal = ['packageName' => 'data_packages.name', 'created_at' => 'data_resources.created_at'];
    protected $sortByMappingData = ['packageName' => 'package_name', 'created_at' => 'created_at'];

    public function __construct(DataResourceTransformer $transformer, ResourceManager $resourceManager)
    {
        $this->transformer = $transformer;
        $this->resourceManager = $resourceManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->input('search');
        $filters = request()->input('filters');
        $sortBy = request()->input('sortBy');
        $orderDesc = (request()->input('order') == 'desc');
        $ids = request()->input('ids');

        if ($ids) {
            $ids = explode(',', $ids);
        }

        $filters = $this->setFilters($filters);

        $maxPagination = config('capstone.frontend.max-pagination', 250);

        $count = (int) request()->input('count');
        if (!$count || $count > $maxPagination) {
            $count = $maxPagination;
        }

        $page = (int) request()->input('page');
        if (!$page) {
            $page = 1;
        }

        if (!in_array($sortBy, $this->validSortBy)) {
          $sortBy = 'packageName';
        }

        $data = collect();
        $providers = explode(',', request()->input('provider'));
        $totalRows = 0;

        $remoteData = collect();

        if (in_array('_remote', $providers)) {
            // Add windowing/pagination to call

            $resourceProvider = $this->resourceManager->getProvider();

            if ($resourceProvider && config('capstone.features.remote-data-resources', false)) {
                // FIXME: at present, because we have no foreknowledge (to avoid retaining external data)
                // and are interleaving local and remote, we have to get rows 1 to $count * page, instead
                // of a smaller number. This gets unboundedly large as page number increases
                // Switching to cursor-based pagination may make this more manageable.
                $sortByRemote = $sortBy;
                if (array_key_exists($sortBy, $this->sortByMappingRemote)) {
                    $sortByRemote = $this->sortByMappingRemote[$sortBy];
                }
                $search = preg_replace("/[^.0-9 a-zA-Z]/", "", $search);
                list($rows, $dataResult) = $resourceProvider->getDataResources('"' . $search . '"', $filters, $sortByRemote, $orderDesc, $count * $page);

                $data = $data->merge($dataResult);
                $totalRows += $rows;
            }
        }

        if ($data->count() === 0 && in_array('_local', $providers)) {
            $query = DataResource::with(['package', 'run'])
                ->leftJoin('data_packages', 'data_packages.id', '=', 'package_id')
                ->select('data_packages.*', 'data_resources.*');

            if ($ids) {
                $query = $query->whereIn('data_resources.id', $ids);
            }
            if ($search) {
                $query = $query->where(function ($query) use ($search) {
                    return $query->where('filename', 'LIKE', '%' . $search . '%')
                        ->orWhereHas('package', function ($query) use ($search) {
                            return $query->where('name', 'LIKE', '%' . $search . '%');
                        });
                });
            }
            foreach ($filters as $filter => $value) {
                if ($filter == 'created_at') {
                    $month = \Carbon\Carbon::parse($value . '-01');
                    $query = $query->whereDate('data_resources.created_at', '>=', $month->format('Y-m-d'));
                    $query = $query->whereDate('data_resources.created_at', '<=', $month->endOfMonth()->format('Y-m-d'));
                } else {
                    $query = $query->where($filter, '=', $value);
                }
            }

            $sortByLocal = $sortBy;
            if (array_key_exists($sortBy, $this->sortByMappingLocal)) {
                $sortByLocal = $this->sortByMappingLocal[$sortBy];
            }

            $query = $query->orderBy($sortByLocal, $orderDesc ? 'desc' : 'asc');
            $rows = $query->count();

            $query
                ->offset(0)
                ->limit($count * $page);

            $localData = $query->get();
            $data = $data->merge($localData);
        }


        if ($orderDesc) {
            $data = $data->sortByDesc(function ($resource) use ($sortBy) {
                return $resource[$this->sortByMappingData[$sortBy]] . '__' . $resource['package_name'] . '__' . $resource['filename'];
            });
        } else {
            $data = $data->sortBy(function ($resource) use ($sortBy) {
                return $resource[$this->sortByMappingData[$sortBy]] . '__' . $resource['package_name'] . '__' . $resource['filename'];
            });
        }

        $data = $data->slice($count * ($page - 1), $count);

        $paginator = new LengthAwarePaginator($data, $totalRows, $count);
        $paginator->setPath('/dataResources/');

        //$users = User::whereIn('id', $data->pluck('user_id')->filter())->get()->each(function (&$user) {
        //  $user->retrieve();
        //})->keyBy('id');

        //$data->each(function (&$data) use ($users) {
        //    if ($data->user_id) {
        //        $data->user = $users[$data->user_id];
        //    }
        //});

        return fractal()
            ->collection($data, $this->transformer, 'dataResources')
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = $request->input('url');
        $user = Auth::user();
        $dataResource = $this->resourceManager->find($url, $user);

        if (!$dataResource) {
            $dataResource = app()->make(DataResource::class);
        }

        $name = $request->input('name');
        if (! $name) {
            $name = basename($url);
            $name = explode('?', $name)[0];
        }

        if (strlen($name) > 200) {
            $name = substr($name, 0, 197) . '...';
        }

        $dataResource->settings = [
          'name' => $name,
          'dataProfileId' => $request->input('profileId')
        ];

        $dataResource->source = $request->input('source');
        $dataResource->organization = '(unknown)';
        $dataResource->url = $request->input('url');

        if ($user) {
            $dataResource->user_id = $user->id;
        }

        $dataResource->filetype = $request->input('filetype');

        $dataResource = $this->resourceManager->onboard($dataResource);

        if ($dataResource) {
            return fractal()
                ->item($dataResource, $this->transformer, 'dataResources')
                ->respond();
        }
        abort(400, __("Invalid data"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataResource  $dataResource
     * @return \Illuminate\Http\Response
     */
    public function show(DataResource $dataResource)
    {
        $fields = request()->get('fields');
        if ($fields && array_key_exists('dataResources', $fields)) {
            $additionalFields = explode(',', $fields['dataResources']);
            if (in_array('remoteMetadata', $additionalFields)) {
                $this->transformer->setOption('withRemoteMetadata', true);
            }
        }

        return fractal()
            ->item($dataResource, $this->transformer, 'dataResources')
            ->respond();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataResource  $dataResource
     * @return \Illuminate\Http\Response
     */
    public function edit(DataResource $dataResource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataResource  $dataResource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataResource $dataResource)
    {
        //
        $dataResource2 = DataResource::findOrFail($dataResource->id);
        $dataResource2->archived = $dataResource->archived;
        $dataResource2->save();
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataResource  $dataResource
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataResource $dataResource)
    {
        $resource = DataResource::findOrFail($dataResource->id);
        $resource->delete();
        
    }

    /**
     * Gets array of values of file types
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getFileTypeFilters(Request $request)
    {
        return response(DataResource::select('filetype')->distinct()->get(), 200);
    }

    /**
     * Gets array of values of organizations
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getOrganizationFilters(Request $request)
    {
        $organizations = collect();
        //$organizations = DataResource::select('organization')
        //    ->distinct()
        //    ->get()
        //    ->pluck('organization');

        $resourceProvider = $this->resourceManager->getProvider();
        if ($resourceProvider && config('capstone.features.remote-data-resources', false)) {
            $remote = $resourceProvider->getOrganizations();
            $organizations = $organizations->concat($remote);
        }
        $organizations = $organizations->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE);

        return response($organizations->values(), 200);
    }

    /**
     * Gets array of values of sources
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSourceFilters(Request $request)
    {
        return response(DataResource::select('source')->distinct()->get(), 200);
    }

    /**
     * Gets array of values of dates
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getDateFilters(Request $request)
    {
        $months = DataResource::distinct()
            ->get([DB::raw('EXTRACT(YEAR FROM Date(created_at)) as year, EXTRACT(MONTH FROM Date(created_at)) as month')]);
        return response($months, 200);
    }

    /**
     * @param $filters
     * @return array
     */
    private function setFilters($filters): array
    {
        if ($filters) {
            $filters = collect(explode(',', $filters))
                ->map(function ($filterString) {
                    $filter = explode(':', $filterString);

                    if (count($filter) !== 2 || !in_array($filter[0], $this->validFilters) || !$filter[1]) {
                        return null;
                    }

                    return [
                        'filter' => $filter[0],
                        'value' => $filter[1]
                    ];
                })
                ->filter()
                ->pluck('value', 'filter')
                ->toArray();
        } else {
            $filters = [];
        }
        return $filters;
    }

    public function summary()
    {
        $from = request()->input('from');
        $to = request()->input('to');
        $createdSince = request()->input('createdSince');

        try {
            if ($from) {
                $from = Carbon::parse($from);
            } else {
                $from = Carbon::now()->subHours(1);
            }

            if ($to) {
                $to = Carbon::parse($to);
            } else {
                $to = Carbon::now();
            }

            if ($createdSince) {
                $createdSince = Carbon::parse($createdSince);
            } else {
                $createdSince = null;
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => __("Invalid from or to dates")
            ];
        }

        $dataResourceModel = app()->make(DataResource::class);
        $validationRunModel = app()->make(ValidationRun::class);

        $trackings = StatusTracking::where('created_at', '>=', $from)
            ->where('created_at', '<', $to)
            ->get();

        $results = [
            'now' => [
                'resource_statuses' => $dataResourceModel->summaryByStatus($createdSince),
                'run_statuses' => $validationRunModel->summaryByStatus($createdSince)
            ]
        ];
        $trackings->each(function ($tracking) use (&$results) {
            $results[$tracking->created_at->format('c')] = [
                'resource_statuses' => $tracking->statuses['resource_statuses'],
                'run_statuses' => $tracking->statuses['run_statuses'],
                'jobs' => $tracking->statuses['jobs']
            ];
        });

        return [
            'success' => true,
            'results' => $results
        ];
    }
}
