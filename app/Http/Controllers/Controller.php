<?php

namespace App\Http\Controllers;

/**
 * From lintol/bedappy-controllers
 */

use App;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Waavi\Sanitizer\Sanitizer;
use Swis\JsonApi\Client\Parsers\DocumentParser;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Sanitize a dataset using given filters.
     *
     * @param array $data
     * @param array $filters
     * @return array
     */
    public function sanitize($request, $filters)
    {
        $data = $this->sanitizeData($request->all(), $filters);

        $request->replace($data);

        return $request;
    }

    /**
     * Get a sanitizer
     *
     * @return Waavi\Sanitizer\Sanitizer
     */
    public function sanitizeData($data, $filters)
    {
        $factory = App::make('sanitizer');

        $sanitizer = $factory->make($data, $filters);

        return $sanitizer->sanitize();
    }

    /**
     * Check whether a value is a UUID
     *
     * @param Illuminate\Http\Request $request
     * @param string $uuid
     * @param string $message
     * @return void
     */
    public function validateUuid(Request $request, $uuid, $message = "Invalid UUID")
    {
        $validator = $this->getValidationFactory()->make(
            ['uuid' => $uuid],
            ['uuid' => 'required|uuid'],
            ['uuid' => $message]
        );

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
    }

    /**
     * Validate the given array with the given rules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $input
     * @param  array  $rules
     * @param  array  $messages
     * @param  array  $customAttributes
     * @return array
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateArray(Request $request, array $input, array $rules,
                             array $messages = [], array $customAttributes = [])
    {
        return $this->getValidationFactory()->make(
            $input, $rules, $messages, $customAttributes
        )->validate();
    }

    public function jsonApiToArray(Request $request, Model $model)
    {
        $input = $request->all();

        if (! array_key_exists('id', $input['data'])) {
            $input['data']['id'] = '[none]';
        }

        if (empty($input['data']['relationships'])) {
            unset($input['data']['relationships']);
        }

        if (empty($input['data']['attributes'])) {
            unset($input['data']['attributes']);
        }

        $parser = app()->make(DocumentParser::class);
        $document = $parser->parse(json_encode($input));
        $data = $document->getData();

        $input = $data->toArray();

        return $input;
    }
}
