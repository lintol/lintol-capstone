<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Lintol\Capstone\Models\Processor;
use Lintol\Capstone\Services\StatusService;
use Lintol\Capstone\Transformers\ProcessorTransformer;
use Lintol\Capstone\Services\ProcessorActionService;
use Lintol\Capstone\Jobs\ProcessorActionJob;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProcessorController extends Controller
{
    /**
     * Initialize the transformer
     */
    public function __construct(ProcessorTransformer $transformer)
    {
        $this->transformer = $transformer;
        $this->authorizeResource(Processor::class, 'processor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StatusService $statusService)
    {
        $statusService->updateProcessorStatuses();

        $query = new Processor;

        $query = collect(request()->get('filter'))
            ->map(function ($value, $key) { return [$key, $value]; })
            ->reduce(function ($query, $value) {
                list($filter, $value) = $value;

                if ($filter === 'onlyMine' && $value) {
                    $query = $query->whereCreatorId(Auth::user()->id);
                }

                return $query;
            }, $query);

        $processors = $query->get();

        return fractal()
            ->collection($processors, $this->transformer, 'processors')
            ->respond();
    }

    /**
     * Display a single processor.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Processor $processor, StatusService $statusService)
    {
        $statusService->updateProcessorStatuses();

        $fields = request()->get('fields');
        if ($fields && array_key_exists('processors', $fields)) {
            $additionalFields = explode(',', $fields['processors']);
            if (in_array('definition', $additionalFields)) {
                $this->transformer->setOption('withDefinition', true);
            }
            if (in_array('supplementaryLinks', $additionalFields)) {
                $this->transformer->setOption('withSupplementary', true);
            }
        }

        return fractal()
            ->item($processor, $this->transformer, 'processors')
            ->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAction($id, Request $request, ProcessorActionService $processorActionService)
    {
        $action = $request->input('action');

        $processor = Processor::findOrFail($id);

        if (!$processorActionService->checkActionValid($action)) {
            throw new HttpException(400, "Invalid processor action");
        }

        ProcessorActionJob::dispatch($processor, $action);
    }

    public function getStatuses(Request $request, StatusService $statusService)
    {
        $statuses - $statusService->getProcessorStatuses();

        return $statuses;
    }

    /**
     * Destroy a processor.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Processor $processor)
    {
        if ($processor->delete()) {
            return fractal()
                ->item($processor, $this->transformer, 'processors')
                ->respond();
        }

        throw new HttpException(500, "Could not delete");
    }

    /**
     * Create/undertake an action as a resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $processor = new Processor;

        $input = $this->jsonApiToArray(
            $request,
            $processor
        );

        $input = array_merge(
            $input,
            ['creatorId' => Auth::user()->id]
        );

        $this->validateArray(
            $request,
            $input,
            $processor->controllerRules()
        );

        $allowedDomains = config('capstone.processors.allowed-supplementary-domains', []);
        $supplementaryLinks = $input['supplementaryLinks'] ?? [];
        foreach ($supplementaryLinks as $source => $link) {
            $url = parse_url($link);
            if (isset($url['host']) && !in_array($url['host'], $allowedDomains)) {
                throw new HttpException(400, "Invalid supplementary URLs");
            }
        }

        /* FIXME: would be neatest to tighten this up */
        $configurationOptions = json_decode($input['configurationOptions'] ?? '{}');
        $configurationDefaults = json_decode($input['configurationDefaults'] ?? '{}');

        $processor->name = $input['name'];
        $processor->creator_id = $input['creatorId'];
        $processor->description = $input['description'] ?? '';
        $processor->definition = json_decode($input['definition'] ?? '{}');

        $processor->supplementary_links = $supplementaryLinks;
        $processor->configuration_options = $configurationOptions;
        $processor->configuration_defaults = $configurationDefaults;

        $processor->module = $input['module'] ?? '';
        $processor->content = $input['content'] ?? '';
        $processor->unique_tag = $input['uniqueTag'];

        if ($processor->save()) {
            return fractal()
                ->item($processor, $this->transformer, 'processors')
                ->respond();
        }

        throw new HttpException(400, "Invalid data");
    }
}
