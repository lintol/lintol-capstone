<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Carbon\Carbon;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Lintol\Capstone\Models\Report;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Services\ReportExportService;
use Lintol\Capstone\Transformers\ReportTransformer;
use Lintol\Capstone\Jobs\ProcessDataJob;
use RuntimeException;

class ReportController extends Controller
{
    protected $validSortBy = ['created_at'];

    /**
     * Initialize the transformer
     */
    public function __construct(ReportTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reports = new Report;

        if ($request->input('since')) {
            $this->validate($request, ['since' => 'required|date']);

            Log::debug("Filtering with since");
            $reports = $reports->where(
                'created_at',
                '>=',
                Carbon::parse($request->input('since'))
            );
        }

        if ($request->input('until')) {
            $this->validate($request, ['until' => 'required|date']);

            Log::debug("Filtering with until");
            $reports = $reports->where(
                'created_at',
                '<',
                Carbon::parse($request->input('until'))
            );
        }

        if ($request->input('entity') && $request->input('id')) {
            $this->validate($request, ['id' => 'uuid']);
            $entity = $request->input('entity');
            $entityId = $request->input('id');

            $cachedOnly = (bool)$request->input('cachedOnly');

            if ($cachedOnly) {
                if ($entity == 'resource') {
                    $reports = $reports->whereCachedDataResourceId($entityId);
                } else if ($entity == 'package') {
                    $reports = $reports->whereCachedDataPackageId($entityId);
                } else if ($entity == 'profile') {
                    $reports = $reports->whereCachedProfileId($entityId);
                } else {
                    return response([
                        'success' => false,
                        'message' => 'Only resource and profile searches are allowed'
                    ], 400);
                }
            } else {
                if ($entity == 'resource') {
                    $runQuery = ValidationRun::whereDataResourceId($entityId);
                } else if ($entity == 'package') {
                    $resourceIds = DataResource::wherePackageId($entityId)->pluck('id');
                    $runQuery = ValidationRun::whereIn('data_resource_id', $resourceIds);
                } else if ($entity == 'profile') {
                    $runQuery = ValidationRun::whereProfileId($entityId);
                } else {
                    return response([
                        'success' => false,
                        'message' => 'Only resource and profile searches are allowed'
                    ], 400);
                }
                $runIds = $runQuery->pluck('id');
                $reports = $reports->whereIn(
                    'run_id',
                    $runIds
                );
            }
        }

        $sortBy = request()->input('sortBy');

        if (!in_array($sortBy, $this->validSortBy)) {
            $sortBy = 'created_at';
        }

        $orderDesc = ! (request()->input('order') == 'asc');
        $reports = $reports->orderBy($sortBy, $orderDesc ? 'desc' : 'asc');

        $maxPagination = config('capstone.frontend.max-pagination', 250);

        $count = (int) request()->input('count');
        if (!$count || $count > $maxPagination) {
            $count = $maxPagination;
        }

        $paginator = $reports->paginate($count);
        $reports = $paginator->getCollection();
        $paginator->setPath('/reports/');

        $response = fractal()
            ->collection($reports, $this->transformer, 'reports')
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->respond();

        return $response;
    }

    /**
     * Display a listing of the resource for machine route.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::findOrFail($id);

        if ($report->owner) {
            $report->owner->retrieve();
        }

        return fractal()
            ->item($report, $this->transformer, 'reports')
            ->respond();
    }

    /**
     * Soft-delete a report.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        if ($report->delete()) {
            return fractal()
                ->item($report, $this->transformer, 'reports')
                ->respond();
        }

        throw new HttpException(500, "Could not delete");
    }

    /**
     * Re-run the specified validation run.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function rerun($id, Request $request)
    {
        $report = Report::findOrFail($id);

        $setting = $request->all();

        $run = $report->run;
        if (! $run) {
            throw RuntimeException(__("This report has no matching validation run."));
        }

        if (array_key_exists('artifacts', $setting)) {
            $artifacts = $setting['artifacts'];

            if (!is_array($artifacts)) {
                abort(400, __("Artifacts must be a string array"));
            }

            foreach ($artifacts as $artifact) {
                if (!is_string($artifact)) {
                    abort(400, __("Artifacts must be a string array"));
                }
            }
        } else {
            $artifacts = false; // Doesn't change from previous run
        }

        $replicate = false;
        if (array_key_exists('replicate', $setting) && $setting['replicate']) {
            $replicate = true;
        }

        $newRun = $run->duplicate($replicate, $artifacts);
        $newRun->save();

        ProcessDataJob::dispatch($newRun->id);

        return $newRun->id;
    }

    public function exportCsv($id, ReportExportService $exporter)
    {
        $user = Auth::user();

        if (! $user->id) {
            // This is a confirmation check and should never be called
            abort(401, __("Authentication required"));
        }

        if (! $user->feature('export-csv')) {
            abort(403, __("Unavailable"));
        }

        $report = Report::findOrFail($id);

        try {
            return $exporter->renderCsv($report);
        } catch (RuntimeException $e) {
            abort(400, $e->getMessage());
        }
    }

    /**
     * Re-run the specified validation run.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function exportArtifact($id, Request $request)
    {
        $report = Report::findOrFail($id);

        $content = json_decode($report->content);

        if (!property_exists($content, 'artifacts')) {
            abort(400, __("Artifacts must be a string array"));
        }

        $tag = $request->get('tag');
        // TODO: use processorConfiguration IDs
        $processor = $request->get('processor');
        if (!$processor || !$tag) {
            abort(400, __("Artifact requests need a report ID, an artifact tag and a processor"));
        }

        $code = $processor . '#' . $tag;
        if (!property_exists($content->artifacts, $code)) {
            abort(404, __("Artifacts not found"));
        }

        $filename = 'report-' . $report->id . '-' . basename($content->artifacts->{$code}->uri);
        switch ($content->artifacts->{$code}->mime) {
        case 'text/csv':
            $filename = $filename . '.csv';
        default:
        }

        return Storage::disk('s3')->download($content->artifacts->{$code}->uri, $filename);
    }
}
