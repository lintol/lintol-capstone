<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Services\StatusService;
use Lintol\Capstone\Transformers\ValidationRunTransformer;
use Lintol\Capstone\Jobs\ProcessDataJob;

class ValidationRunController extends Controller
{
    protected $validSortBy = ['created_at'];

    /**
     * Initialize the transformer
     */
    public function __construct(ValidationRunTransformer $transformer, StatusService $statusService)
    {
        $this->transformer = $transformer;
        $this->statusService = $statusService;
    }

    /**
     * Display a listing of the validation runs actively incomplete.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function indexIncomplete(Request $request)
    {
        $this->statusService->updateProcessorStatuses();
        $activeRunIds = $this->statusService->getIncompleteRuns();
        $runs = new ValidationRun;

        // TODO: filter by owner

        $runs = $runs->whereIn('id', $activeRunIds);

        $runs = $runs->get();

        $response = fractal()
            ->collection($runs, $this->transformer, 'runs')
            ->respond();

        return $response;
    }
}
