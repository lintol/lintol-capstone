<?php

namespace App\Policies;

use App\User;
use Lintol\Capstone\Models\Processor;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProcessorPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any processors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the processor.
     *
     * @param  \App\User  $user
     * @param  \App\Processor  $processor
     * @return mixed
     */
    public function view(User $user, Processor $processor)
    {
        return true;
    }

    /**
     * Determine whether the user can create processors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the processor.
     *
     * @param  \App\User  $user
     * @param  \App\Processor  $processor
     * @return mixed
     */
    public function update(User $user, Processor $processor)
    {
        return $processor->creator_id == $user->id;
    }

    /**
     * Determine whether the user can delete the processor.
     *
     * @param  \App\User  $user
     * @param  \App\Processor  $processor
     * @return mixed
     */
    public function delete(User $user, Processor $processor)
    {
        return $processor->creator_id == $user->id;
    }

    /**
     * Determine whether the user can restore the processor.
     *
     * @param  \App\User  $user
     * @param  \App\Processor  $processor
     * @return mixed
     */
    public function restore(User $user, Processor $processor)
    {
        return $processor->creator_id == $user->id;
    }

    /**
     * Determine whether the user can permanently delete the processor.
     *
     * @param  \App\User  $user
     * @param  \App\Processor  $processor
     * @return mixed
     */
    public function forceDelete(User $user, Processor $processor)
    {
        return $processor->creator_id == $user->id;
    }
}
