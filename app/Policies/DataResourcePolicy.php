<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DataResourcePolicy
{
    use HandlesAuthorization;

    public function show()
    {
        return true;
    }
}
