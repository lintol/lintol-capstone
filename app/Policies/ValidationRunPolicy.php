<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ValidationRunPolicy
{
    use HandlesAuthorization;

    public function show()
    {
        return true;
    }
}
