<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('data-resource.{id}', function ($user, $id) {
    if (substr($id, 0, 7) == 'remote-') {
        $remoteId = substr($id, 7);
        \Log::info($remoteId);
        \Log::info($id);
        $resource = \Lintol\Capstone\Models\DataResource::firstOrNew([
            'remote_id' => $remoteId
        ]);
    } else {
        $resource = \Lintol\Capstone\Models\DataResource::findOrNew($id);
    }
    return $user->can('show', $resource);
});
