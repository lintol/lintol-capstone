<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$group = [
    'prefix' => 'v1.0'
];

$machineGroup = [
    'prefix' => 'v1.0'
];

if (!env('CAPSTONE_WITHOUT_AUTH', false)) {
    $group['middleware'] = 'auth:api';
    $machineGroup['middleware'] = 'client';
}

//Route::get('reports/all', 'ReportController@all');

//Route::group($machineGroup, function () {
//    Route::resource('dataResources', 'DataResourceController', [
//        'only' => ['all']
//    ]);
//    Route::resource('reports', 'ReportController', [
//        'only' => ['all']
//    ]);
//});

Route::group($group, function () {
    Route::get('users/me', 'UserController@me');

    Route::resource('profiles', 'ProfileController', [
        'only' => ['index', 'show', 'update', 'store']
    ]);
    Route::get('runs/all/incomplete', 'ValidationRunController@indexIncomplete');
    Route::resource('processors', 'ProcessorController', [
        'only' => ['index', 'store', 'destroy', 'show']
    ]);
    Route::get('processors/statuses', 'ProcessorController@getStatuses');
    Route::resource('reports', 'ReportController', [
        'only' => ['index', 'show', 'destroy']
    ]);
    Route::post('processors/{id}/actions', 'ProcessorController@storeAction');

    Route::post('reports/{id}/rerun', 'ReportController@rerun');
    Route::get('reports/{id}/artifact', 'ReportController@retrieveArtifact');
    Route::resource('dataResources', 'DataResourceController', [
        'only' => ['index', 'show', 'store', 'update', 'destroy']
    ]);
    Route::resource('dataPackages', 'DataPackageController', [
        'only' => ['show']
    ]);

    Route::get('dataResourcesFilters/getFileTypeFilters', 'DataResourceController@getFileTypeFilters')->name('getFileTypeFilters');
    Route::get('dataResourcesFilters/getOrganizationFilters', 'DataResourceController@getOrganizationFilters')->name('getFileTypeFilters');
    Route::get('dataResourcesFilters/getSourceFilters', 'DataResourceController@getSourceFilters')->name('getSourceFilters');
    Route::get('dataResourcesFilters/getDateFilters', 'DataResourceController@getDateFilters')->name('getDateFilters');
    Route::get('dataResources/all/summary', 'DataResourceController@summary');

    Route::get('statistics/entities', 'StatisticsController@entities');

    Route::get('tracking', function () {
        return \App\StatusTracking::where('created_at', '>', \Carbon\Carbon::now()->subHours(24))->get();
    });

    Route::resource('dataResources/settings', 'DataResourceSettingController', [
        'only' => ['store']
    ]);
    /* Will enable when pagination in the front end is working 
    Route::get('dataResources', function () {
      return Lintol\Capstone\Models\DataResource::paginate();
    }); */
    Route::resource('users', 'UserController', [
        'only' => ['index']
    ]);
});
