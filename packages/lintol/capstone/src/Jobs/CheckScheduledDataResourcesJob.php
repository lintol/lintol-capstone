<?php

namespace Lintol\Capstone\Jobs;

use File;
use App;
use Carbon\Carbon;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Models\Processor;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\ValidationProcess;
use Lintol\Capstone\ResourceManager;

use Thruway\ClientSession;
use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Lintol\Capstone\WampConnection;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Lintol\Capstone\ValidationExceptionThrottled;

class CheckScheduledDataResourcesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $wampConnection;

    public function __construct() {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ResourceManager $resourceManager)
    {
        Log::info(__("Checking for scheduled datasets"));
        try {
            $resourcesToCheck = DataResource::whereNotNull('run_schedule')
                ->get()
                ->filter(function ($dataResource) {
                    return $dataResource->dueToBeChecked();
                })
                ->each(function ($dataResource) use ($resourceManager) {
                    $dataResource->last_scheduled_run = Carbon::now();
                    $dataResource->save();
                    CheckForUpdateJob::dispatch($dataResource);
                });

            Log::info(__("Subscription exited."));
        } catch (\Exception $e) {
            Log::error(__("EXCEPTION CAUGHT."));
            Log::error($e->getMessage());
        }
    }

}
