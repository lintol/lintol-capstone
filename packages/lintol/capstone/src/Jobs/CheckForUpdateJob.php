<?php

namespace Lintol\Capstone\Jobs;

use File;
use App;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Models\Processor;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\ValidationProcess;
use Lintol\Capstone\Jobs\StatusRetrieveJob;
use Lintol\Capstone\ResourceManager;

use Thruway\ClientSession;
use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Lintol\Capstone\WampConnection;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Lintol\Capstone\ValidationExceptionThrottled;

class CheckForUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $dataResource = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(DataResource $dataResource)
    {
        $this->dataResource = $dataResource;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ResourceManager $resourceManager)
    {
        $dataResource = $this->dataResource;
        $resourceManager->onboardRedirectable($dataResource, true, true);
        $dataResource->save();
        Log::info(__("Client exited"));
    }

    public function tags()
    {
        return ['validation-outgoing', 'process'];
    }
}
