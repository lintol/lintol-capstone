<?php

namespace Lintol\Capstone\Jobs;

use File;
use App;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Models\Processor;
use Lintol\Capstone\Models\Data;
use Lintol\Capstone\ValidationProcess;

use Thruway\ClientSession;
use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Lintol\Capstone\WampConnection;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Lintol\Capstone\ValidationExceptionThrottled;

class ProcessorActionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $wampConnection;

    protected $processor;

    protected $action;

    public function __construct(Processor $processor, string $action) {
        $this->processor = $processor;
        $this->action = $action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WampConnection $wampConnection)
    {
        Log::info(__("Job running for processor action: " . $this->action));

        $wampConnection->execute(function (ClientSession $session) {
            Log::info(__("Engaging for processor action: " . $this->action));
            return $session->call('com.ltldoorstep.engage')
            ->then(
                function ($res) use (&$session) {
                    Log::info('PA engaged...');
                    Log::info($this->processor->unique_tag . ' - ' . $this->action);
                    Log::info('(server: ' . $res[0][0] . ' ; session: ' . $res[0][1] . ')');
                    $uri = 'com.ltldoorstep.' . $res[0][0] . '.processor.action';
                    Log::info($uri);
                    return $session->call($uri, [
                        $res[0][1],
                        $this->action,
                        $this->processor->unique_tag,
                        $this->processor->definition
                    ])->then(function ($res) {
                        Log::info('Called processor action');
                    }, function ($err) {
                        Log::info(get_class($error));
                        Log::info($error);
                    });
                }
            )->then(function ($res) {
                Log::info('Finished processor action');
            });
        });

        Log::info(__("Client exited"));
    }
}
