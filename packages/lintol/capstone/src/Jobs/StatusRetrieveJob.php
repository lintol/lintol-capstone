<?php

namespace Lintol\Capstone\Jobs;

use File;
use App;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Models\Processor;
use Lintol\Capstone\Models\Data;
use Lintol\Capstone\ValidationProcess;

use Thruway\ClientSession;
use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Lintol\Capstone\WampConnection;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Lintol\Capstone\ValidationExceptionThrottled;

class StatusRetrieveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $wampConnection;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WampConnection $wampConnection)
    {
        Log::info(__("Job running for status retrieval"));

        $wampConnection->execute(function (ClientSession $session) {
            return $session->publish('com.ltldoorstep.status-retrieve');
        });

        Log::info(__("Client exited"));
    }
}
