<?php

namespace Lintol\Capstone\Services;

use Cache;
use Lintol\Capstone\Models\ValidationRun;

class StatusService
{
    protected $engine;

    protected $processorStatuses = false;

    protected $incompleteRuns = [];

    public function getActiveServers()
    {
        return Cache::tags(['ltldoorstep'])->get('servers', []);
    }

    public function getIncompleteRuns()
    {
        return $this->incompleteRuns;
    }

    public function getProcessorStatus($processor)
    {
        if ($this->processorStatuses) {
            $statistics = [
                'available' => 0,
                'total' => 0
            ];
            $found = false;
            foreach ($this->processorStatuses as $key => $status) {
                $matches = [];

                // This checks if a direct match, or e.g. "/lintol/test.*/" matches "lintol/test",
                // by stripping it to "/lintol\/test./"
                $uniqueTag = $status['uniqueTag'];
                $function = $status['function'];
                if (
                  $processor->unique_tag == $uniqueTag ||
                    ($uniqueTag[0] == '/' && substr($uniqueTag, -1) == '/' &&
                     preg_match('/' . str_replace('/', '\/', substr($uniqueTag, 1, -1)) . '/', $processor->unique_tag, $matches, PREG_OFFSET_CAPTURE) &&
                     $matches[0][1] == 0 &&
                     is_array($processor->definition) && array_key_exists('docker', $processor->definition) &&
                     array_key_exists('image', $processor->definition['docker']) &&
                     $processor->definition['docker']['image'] == $function)
                ) {
                    $statistics['available'] += $status['available'];
                    $statistics['total'] += $status['total'];
                    $found = true;
                }
            }
            if ($found) {
                return $statistics;
            }
        }

        return false;
    }

    public function updateProcessorStatuses()
    {
        $servers = $this->getActiveServers();

        $processors = [];
        \Log::info((array)$servers);
        foreach ($servers as $serverId) {
            $status = Cache::tags(['ltldoorstep', 'status'])->get($serverId, []);

            foreach ($status as $match) {
                $uniqueTag = $match[0];
                $processor = $match[1];
                if (! array_key_exists($uniqueTag . '|' . $processor->name, $processors)) {
                    $processors[$uniqueTag . '|' . $processor->name] = [
                        'available' => 0,
                        'total' => 0,
                        'uniqueTag' => $uniqueTag,
                        'function' => $processor->name
                    ];
                }
                $processors[$uniqueTag . '|' . $processor->name]['available'] += $processor->available;
                $processors[$uniqueTag . '|' . $processor->name]['total'] += $processor->total;
            }

            $sessions = Cache::tags(['ltldoorstep', 'sessions'])->get($serverId . '-sessions', []);
            \Log::info($sessions);
            $runs = ValidationRun::whereIn('doorstep_session_id', $sessions)
                ->whereCompletionStatus(ValidationRun::STATUS_RUNNING)
                ->pluck('id')->toArray();

            $this->processorStatuses = $processors;
            $this->incompleteRuns = $runs;
            \Log::info($runs);
        }
    }
}
