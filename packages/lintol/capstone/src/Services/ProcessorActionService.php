<?php

namespace Lintol\Capstone\Services;

use Cache;
use Lintol\Capstone\Models\ValidationRun;

class ProcessorActionService
{

    public static function checkActionValid(string $action) {
        return $action === 'restart';
    }
}
