<?php

namespace Lintol\Capstone\Services\Rules;

class SizeMatch
{
    public function apply(array $metadata, array $rules)
    {
        $maxSize = false;
        if (array_key_exists('maxSizeManual', $rules)) {
            $maxSize = $rules['maxSizeManual'];
        }
        if (array_key_exists('maxSize', $rules) &&
                array_key_exists('autorun', $metadata) && $metadata['autorun']) {
            $maxSize = $rules['maxSize'];
        }
        if ($maxSize !== false) {
            if (array_key_exists('size', $metadata)) {
                return $metadata['size'] && $metadata['size'] < $maxSize;
            }
            return false;
        }

        return true;
    }
}
