<?php

namespace Lintol\Capstone\Services;

use RuntimeException;
use SplTempFileObject;
use League\Csv\Writer;
use Lintol\Capstone\Models\Report;

class TabularReportIssue
{
    public const LEVEL_ERROR   = 'ERR';
    public const LEVEL_INFO    = 'INF';
    public const LEVEL_WARNING = 'WNG';

    protected $row = null;

    protected $column = null;

    protected $type = null;

    protected $level = null;

    public $issue;

    public function __construct($issue, $level) {
        $this->issue = $issue;
        $this->level = $level;
        $this->updateLocation();
    }

    public function getExportColumns() {
        $errorData = $this->getErrorData();
        if ($errorData && property_exists($errorData, 'export-columns')) {
            return $errorData->{'export-columns'};
        }
        return [];
    }

    public function getErrorData() {
        if (property_exists($this->issue, 'error-data')) {
            return $this->issue->{'error-data'};
        }
        return null;
    }

    public function getRow() {
        return $this->row;
    }

    public function getColumn() {
        return $this->column;
    }

    public function getCode() {
        if (property_exists($this->issue, 'code')) {
            return $this->issue->code;
        }

        return '?';
    }

    public function getProcessor() {
        if (property_exists($this->issue, 'processor')) {
            return $this->issue->processor;
        }

        return '?';
    }

    public function getWholeRow() {
        if ($this->type == 'Cell' && property_exists($this->issue, 'context')) {
            foreach ($this->issue->context as $context) {
                if (property_exists($context, 'entity')
                        && property_exists($context->entity, 'type')
                        && property_exists($context->entity, 'definition')
                        && $context->entity->type === 'Row') {
                    return $context->entity->definition;
                }
            }
            return null;
        }

        if ($this->type == 'Row' && property_exists($this->issue->entity, 'definition')) {
            return $this->issue->entity->definition;
        }
    }

    public function getMessage() {
        if (property_exists($this->issue, 'message')) {
            return $this->issue->message;
        }

        return '';
    }

    protected function updateLocation() {
        $issue = $this->issue;

        $hasItem = property_exists($issue, 'item');

        if ($hasItem) {
            $hasEntity = property_exists($issue->item, 'entity');

            if ($hasEntity) {
                $entity = $issue->item->entity;
                $hasLocation = property_exists($entity, 'location');

                if ($hasLocation) {
                    $hasRow = property_exists($entity->location, 'row');
                    $hasColumn = property_exists($entity->location, 'column');

                    if ($hasRow) {
                        $this->row = $entity->location->row;
                    }

                    if ($hasColumn) {
                        $this->column = $entity->location->column;
                    }
                }

                $hasType = property_exists($entity, 'type');

                if ($hasType) {
                    $this->type = $entity->type;
                }
            }
        }
    }
}

class ReportExportService
{
    public function renderCsv(Report $report)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());

        $reportObj = json_decode($report->content);

        if ($reportObj->preset !== 'tabular') {
            throw new RuntimeException(__("Only tabular reports may currently be exported as CSV"));
        }

        if (count($reportObj->tables) !== 1) {
            throw new RuntimeException(__("Only tabular reports with exactly one table may currently be exported as CSV"));
        }

        $table = $reportObj->tables[0];

        $sortedTable = [];

        $levelSets = [
            TabularReportIssue::LEVEL_ERROR => $table->errors,
            TabularReportIssue::LEVEL_INFO => $table->informations,
            TabularReportIssue::LEVEL_WARNING => $table->warnings,
        ];

        foreach ($levelSets as $level => $levelSet) {
            foreach ($levelSet as $issue) {
                $issueObj = new TabularReportIssue($issue, $level);

                $row = $issueObj->getRow();

                if ($row === null) {
                    $row = -1;
                }

                $sortedTable[] = [$row, $level, $issueObj];
            }
        }

        usort($sortedTable, function ($a, $b) {
            return ($a[0] == $b[0] ? 0 : ($a[0] < $b[0] ? -1 : 1));
        });

        $exportColumns = array_reduce($sortedTable, function ($exportColumns, $issueRow) {
            return array_merge($exportColumns, $issueRow[2]->getExportColumns());
        }, []);
        \Log::info($exportColumns);

        $csv->insertOne(array_merge(
            ['ROW', 'LEVEL', 'MESSAGE', 'CODE', 'PROCESSOR', ''],
            property_exists($table, 'headers') ? $table->headers : []
        ));

        array_map(function ($issueRow) use ($csv) {
            list($row, $level, $issue) = $issueRow;

            if ($row < 0) {
                $row = '';
            }

            $message = $issue->getMessage();
            $code = $issue->getCode();
            $processor = $issue->getProcessor();
            $csv->insertOne(
                array_merge(
                    [$row, $level, $message, $code, $processor, ''],
                    $issue->getWholeRow() ?? []
                )
            );
        }, $sortedTable);

        $run = $report->run;
        $dataResource = $run->dataResource;

        return $csv->output('LTL-REPORT-' . $dataResource->filename);
    }
}
