<?php

namespace Lintol\Capstone;

use Thruway\Peer\Client;

class WampClient extends Client
{
    public function onSessionLeave($args, $kwArgs, $options)
    {
        \Log::warn(_("Disconnected from WAMP"));
    }
}
