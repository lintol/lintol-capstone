<?php

namespace Lintol\Capstone;

use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Log;
use Lintol\Capstone\ResourceManager;
use Socialite;
use Silex\ckan\CkanClient;
use Illuminate\Support\Collection;
use Lintol\Capstone\Models\CkanInstance;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\Models\DataPackage;
use App\RemoteUser;
use App\User;
use Hash;

class CkanResourceProvider implements ResourceProviderInterface
{
    protected $driver;

    private $remoteUser;

    private $ckanClient;

    protected $ckanInstance;

    public function __construct(ResourceManager $resourceManager)
    {
        /* This should be the only way of setting user credentials */
        $user = Auth::user();

        if ($user && $user->primaryRemoteUser && $user->primaryRemoteUser->driver === 'ckan') {
            $this->remoteUser = $user->primaryRemoteUser;
            $this->driver = $resourceManager->getOAuthDriver('ckan', $this->remoteUser->resourceable);
            $this->user = $user;
        } else {
            throw RuntimeException(__(
                "Attempt to create a CKAN resource provider " .
                "without CKAN credentials on logged in user"
            ));
        }
    }

    public static function generate(CkanInstance $ckanInstance)
    {
        $resourceProvider = app(self::class);

        $resourceProvider->setCkanInstance($ckanInstance);

        return $resourceProvider;
    }

    public function loadApiKey()
    {
        if (!$this->ckanClient) {
            if ($this->remoteUser && $this->remoteUser->remote_token) {
                $apiKey = $this->driver->getApiKeyByToken($this->remoteUser->remote_token);
            }

            if (!$apiKey) {
                throw RuntimeException(__(
                    "This resource provider did not successfully" .
                    " retrieve an API key from credentials."
                ));
            }

            $this->ckanClientApiKey = $apiKey;
            $this->ckanClientHttp = new \GuzzleHttp\Client([
                'base_uri' => $this->ckanInstance->uri
            ]);

            $this->ckanClient = CkanClient::factory([
                'baseUrl' => $this->ckanInstance->uri . '/api',
                'apiKey' => $apiKey
            ]);
        }
    }

    public function setCkanInstance($ckanInstance)
    {
        $this->ckanInstance = $ckanInstance;
    }

    public function getOrganizations(): array
    {
        $this->loadApiKey();
        $user = $this->user;
        try {
            $organizationList = $this->ckanClientHttp->request(
                'GET',
                '/api/action/organization_list_for_user',
                array_merge(
                    [
                        'id' => $user->primary_remote_user_id,
                        'headers' => ['X-CKAN-API-Key' => $this->ckanClientApiKey]
                    ],
                    config('capstone.http.ckan.request-options', [])
                )
            );
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            if ($e->getResponse()->getStatusCode() != 502) {
                throw $e;
            }
        }
        $organizations = json_decode($organizationList->getBody(), true);

        if ($organizations) {
            $ckanData = collect($organizations['result'])->map(function ($organization) {
                $title = $organization['title'] ?? $organization['name'];
                return [
                    'name' => $organization['name'],
                    'title' => (trim($title) ? $title : $organization['name'])
                ];
            });
        } else {
            $ckanData = collect();
        }

        return $ckanData->toArray();
    }

    public function getDataResources($search = '', $filters = [], $sortBy = 'title', $orderDesc = false, $limit = 50, $offset = 0, $maxExtensionRetries = 5): array
    {
        if (strlen($search) < 4 && count($filters) == 0) {
            return [0, collect()];
        }
        $this->loadApiKey();

        $user = $this->user;
        $query = ['url' => '.', 'rows' => $limit, 'start' => $offset];
        if ($search) {
            $query['url'] = preg_replace('[^A-Za-z0-9_-.]', '', $search);
        }
        $allowedFormats = ['csv', 'json', 'geojson', 'xml'];
        if (array_key_exists('filetype', $filters) && in_array(strtolower($filters['filetype']), $allowedFormats)) {
            $query['format'] = strtolower($filters['filetype']);
        }

        $ckanQuery = [];
        foreach ($query as $key => $value) {
            $ckanQuery[] = $key . ':' . $value;
        }

        $ckanPQuery = ['q' => str_replace(' ', '&', $search), 'rows' => $limit, 'start' => $offset, 'fq' => []];

        if (isset($filters['organization'])) {
            $ckanPQuery['fq']['organization'] = $filters['organization'];
        }

        if ($sortBy) {
            switch ($sortBy) {
                case 'filename':
                    $ckanQuery['sort'] = 'url';
                    $ckanPQuery['sort'] = 'url';
                    break;
                case 'title':
                    $ckanQuery['sort'] = 'title_string';
                    $ckanPQuery['sort'] = 'title_string';
                    break;
                case 'created':
                    $ckanQuery['sort'] = 'created';

                    if ($orderDesc) {
                        $ckanPQuery['sort'] = 'last_modified';
                    } else {
                        $ckanPQuery['sort'] = 'created';
                    }
                    break;
                default:
                    $sortBy = null;
            }
            if ($sortBy) {
                if ($orderDesc) {
                    $ckanPQuery['sort'] .= ' desc';
                } else {
                    $ckanPQuery['sort'] .= ' asc';
                }
            }
        }

        $ckanPQuery['fq'] = implode(',',
            array_map(
                function ($k, $v) { return $k . ':' . $v; },
                array_keys($ckanPQuery['fq']),
                array_values($ckanPQuery['fq'])
            )
        );

        $count = 0;
        $ckanData = collect();
        $finished = false;
        for ($j = 0; $j < $maxExtensionRetries && !$finished && $count < $limit; $j++) {
            for ($i = 0; $i < 3; $i++) {
                try {
                    $packageSearch = $this->ckanClientHttp->request(
                        'GET',
                        '/api/action/package_search',
                        array_merge(
                            [
                                'query' => $ckanPQuery,
                                'headers' => ['X-CKAN-API-Key' => $this->ckanClientApiKey]
                            ],
                            config('capstone.http.ckan.request-options', [])
                        )
                    );
                    break;
                } catch (\GuzzleHttp\Exception\ServerException $e) {
                    if ($e->getResponse()->getStatusCode() != 502) {
                        throw $e;
                    }
                }
            }
            $packageSearch = json_decode($packageSearch->getBody(), true);
            $finished = count($packageSearch['result']['results']) < $limit;

            $localData = $this->ckanInstance->resources()->with(['package', 'run'])
                ->leftJoin('data_packages', 'data_packages.id', '=', 'package_id')
                ->select('data_packages.*', 'data_resources.*');

            if ($packageSearch) {
                $iterationData = collect($packageSearch['result']['results'])
                    ->map(function ($ckanPackageData) use ($localData, $user, &$count, $filters) {
                        return collect($ckanPackageData['resources'])->filter(function ($ckanData) use ($filters) {
                            if (array_key_exists('filetype', $filters)) {
                                if (strtolower($ckanData['format']) !== strtolower($filters['filetype'])) {
                                    return false;
                                }
                            }

                            if (array_key_exists('created_at', $filters) && preg_match('/\d\d\d\d-\d\d/', $filters['created_at'])) {
                                if (isset($ckanData['created']) && ($date = Carbon::parse($ckanData['created']))) {
                                    $filterDate = Carbon::parse($filters['created_at'] . '-01');
                                    if ($date->month !== $filterDate->month || $date->year !== $filterDate->year) {
                                        return false;
                                    }
                                } else {
                                    return false;
                                }
                            }

                            return true;
                        })->map(function ($ckanData) use ($ckanPackageData) {
                            $data = new DataResource;
                            $data->url = $ckanData['url'];
                            if (isset($ckanPackageData['organization'])) {
                                $data->organization = $ckanPackageData['organization']['name'];
                            }
                            $data->remote_id = $ckanData['id'];
                            $data->source = 'CKAN';
                            $data->filetype = strtolower($ckanData['format']) ? strtolower($ckanData['format']) : '';
                            $data->archived = 0;
                            $parts = parse_url($ckanData['url']);

                            if (array_key_exists('path', $parts)) {
                                $data->filename = basename($parts['path']);
                                if (!str_contains(strtolower($data->filename), $data->filetype)) {
                                    $data->filename .= '.' . $data->filetype;
                                }
                                $data->setStatus('valid link');
                            }

                            $package = new DataPackage;
                            $package->name = $ckanPackageData['title'];
                            $package->remote_id = $ckanPackageData['id'];
                            $data->package = $package;
                            $data->resourceable = $this->ckanInstance;
                            $data->created_at = $ckanData['created'] ? Carbon::parse($ckanData['created']) : null;
                            $data->updated_at = $ckanData['last_modified'] ? Carbon::parse($ckanData['last_modified']) : null;
                            $data->remote_metadata = $ckanData;
                            if ($ckanData['format']) {
                                $data->filetype = strtolower($ckanData['format']);
                            }

                            return $data;
                        });
                    })
                    ->flatten()
                    ->tap(function ($ckanData) use (&$localData) {
                        $localData = $localData
                            ->whereIn('data_resources.remote_id', $ckanData->pluck('remote_id'))
                            ->get()->keyBy('remote_id');
                    })
                    ->map(function ($ckanData) use ($localData, $user) {
                        $data = $localData->get($ckanData->remote_id);

                        if ($data) {
                            // FIXME: temporary solution to legacy data onboarding
                            if (!$data->remote_metadata) {
                                $data->remote_metadata = $ckanData->remote_metadata;
                                $data->save();
                            }
                            $data->remote_metadata = $ckanData->remote_metadata;
                            if ($ckanData->filetype) {
                                $data->filetype = $ckanData->filetype;
                            }
                            return $data;
                        }

                        return $ckanData;
                    });
                $ckanData = $ckanData->merge($iterationData);
                $count = $ckanData->count();
            } else {
                break;
            }
            $ckanPQuery['start'] += $limit;
        }
        $ckanData = $ckanData->sortBy(function($data) use ($ckanQuery) {
            $value = '';
            if ($data->remote_metadata && isset($data->remote_metadata[$ckanQuery['sort']])) {
                $value = $data->remote_metadata[$ckanQuery['sort']];
                if ($ckanQuery['sort'] === 'created') {
                    $value = Carbon::parse($value)->timestamp;
                }
            }
            return $value . '__' . $data->package->name . '__' . $data->filename;
        }, SORT_REGULAR, $orderDesc);

        return [$count, $ckanData];
    }

    public function getDataResource($id)
    {
        $this->loadApiKey();

        $user = $this->user;
        $data = $this->ckanInstance->resources()->whereUserId($user->id)->whereRemoteId($id)->first();

        if (!$data) {
            $data = new DataResource;
        }

        $ckanData = $this->ckanClient->ResourceShow(['id' => $id]);
        $ckanPackageData = $this->ckanClient->PackageShow(['id' => $ckanData['result']['package_id']]);

        $sourceObject = ['lintolSource' => $this->ckanInstance->uri, 'lintolCkanInstanceId' => $this->ckanInstance->id];
        if (array_key_exists('extras', $ckanPackageData)) {
            foreach ($ckanPackageData['extras'] as $extra) {
                if ($extra['key'] == 'harvest_title') {
                    $sourceObject['harvestTitle'] = $extra['value'];
                }
                if ($extra['key'] == 'harvest_source') {
                    $sourceObject['harvestSource'] = $extra['value'];
                }
                if ($extra['key'] == 'harvest_url') {
                    $sourceObject['sourceUrl'] = $extra['value'];
                }
                if ($extra['key'] == 'default_locale' && $extra['value']) {
                    $locale = $extra['value'];
                }
            }
        }

        if ($ckanData) {
            $package = new DataPackage;
            $ckanPackageData = $ckanPackageData['result'];
            $package->name = $ckanPackageData['title'];
            $package->metadata = $ckanPackageData;
            $package->url = $ckanPackageData['url'];
            $package->source = $sourceObject;
            $package->remote_id = $ckanPackageData['id'];
            $package->user_id = $user->id;
            $ckanData = $ckanData['result'];
            $data->remote_id = $ckanData['id'];
            $data->url = $ckanData['url'];
            $data->user_id = $user->id;
            $data->source = $sourceObject;
            $data->remote_metadata = $ckanData;
            $organization = $ckanPackageData['organization'];
            $data->organization = $organization['name'];
            $data->filetype = strtolower($ckanData['format']) ? strtolower($ckanData['format']) : 'csv';
            $parts = parse_url($ckanData['url']);
            $data->filename = basename($parts['path']);
            if (!str_contains(strtolower($data->filename), $data->filetype)) {
                $data->filename .= '.' . $data->filetype;
            }
            $data->archived = 0;
            $data->setStatus('valid link');
            $data->resourceable()->associate($this->ckanInstance);
            $data->package()->associate($package);

            return $data;
        }

        return null;
    }

    public function getUsers(): Collection
    {
        $this->loadApiKey();

        /* TODO: cover clashing hashes case */
        $localUsers = User::leftJoin('remote_users', 'users.primary_remote_user_id', '=', 'remote_users.id')
            ->where('remote_users.driver', '=', 'ckan')
            ->whereNotNull('remote_users.remote_id_hash')
            ->get()->keyBy('remote_id_hash');

        $hashKey = config('capstone.encryption.blind-index-key');
        $ckanUsers = collect($this->ckanClient->GetUsers()->get('result'))
            ->map(function ($ckanUser) use ($localUsers, $hashKey) {
                $hash = hash_hmac('sha256', $ckanUser['id'], $hashKey);

                $user = $localUsers->get($hash);

                if (!$user) {
                    $remoteUser = new RemoteUser;
                    $remoteUser->driver = 'ckan';
                    $user = new User;
                    $user->primaryRemoteUser = $remoteUser;
                    $user->id = 'remote-' . $ckanUser['id'];
                }

                $user->primaryRemoteUser->remoteEmail = $ckanUser['email'];

                return $user;
            });


        return $ckanUsers;
    }
}
