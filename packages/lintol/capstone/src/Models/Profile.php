<?php

namespace Lintol\Capstone\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Lintol\Capstone\Services\RulesService;

class Profile extends Model
{
    use UuidModelTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_RETIRED = 3;

    protected $fillable = [
        'name',
        'description',
        'version',
        'unique_tag',
        'rules'
    ];

    protected $casts = [
        'rules' => 'json'
    ];

    public function configurations()
    {
        return $this->hasMany(ProcessorConfiguration::class);
    }

    public function filters()
    {
        return [];
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function runs()
    {
        return $this->hasMany(ValidationRun::class);
    }

    public static function rules()
    {
        return [];
    }

    public function match($definition, &$unmatched)
    {
        // TODO: make a singleton
        $this->rulesService = app()->make(RulesService::class);
        $rules = Profile::select(['id', 'rules'])->get();
        \Log::info('Matching: ' . $this->name);
        $profiles = $this->rulesService->match($definition, $rules, $unmatched);

        return Profile::whereIn(
            'id',
            $profiles
        )->get();
    }

    public function configurationsByRules($definition, &$unmatched)
    {
        $this->rulesService = app()->make(RulesService::class);

        \Log::info(__("Checking configurations for passes: ") . $this->configurations->count());

        $configurations = $this->configurations->filter(function ($configuration) use ($definition, &$unmatched) {
            $result = $this->rulesService->filter($definition, $configuration->rules, $unmatched);
            if ($result) {
              \Log::info(__("Configuration passed rules"));
            } else {
              \Log::info(__("Configuration failed rules"));
            }
            return $result;
        });

        // Check if we are required to do all or none
        if (array_key_exists('allProcessors', $this->rules) && $this->rules['allProcessors']) {
            if ($configurations->count() < $this->configurations->count()) {
                return collect();
            }
        }

        return $configurations;
    }

    public function buildDefinitions($definition, &$unmatched)
    {
        return $this->configurationsByRules($definition, $unmatched)
            ->keyBy('id')
            ->map(function ($configuration) {
                return $configuration->buildDefinition();
            })
            ->toArray();
    }
}
