<?php

namespace Lintol\Capstone\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use App\User;
use Lintol\Capstone\Services\StatusService;

class Processor extends Model
{
    use UuidModelTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_RETIRED = 3;

    protected $casts = [
        'rules' => 'json',
        'definition' => 'json',
        'supplementary_links' => 'json',
        'artifacts_available' => 'json',
        'configuration_options' => 'json',
        'configuration_defaults' => 'json'
    ];

    protected $fillable = [
         'name',
         'description',
         'unique_tag',
         'module',
         'content',
         'rules',
         'definition',
         'supplementary_links',
         'artifacts_available',
         'configuration_options',
         'configuration_defaults'
    ];

    public function controllerRules()
    {
        return [
            'uniqueTag' => 'required|unique:processors,unique_tag',
            'name' => 'required|min:3',
            'creatorId' => 'required|exists:users,id'
        ];
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function configurations()
    {
        return $this->hasMany(ProcessorConfiguration::class);
    }

    public function getStatusAttribute()
    {
        $statusService = app()->make(StatusService::class);
        if ($this->unique_tag) {
            return $statusService->getProcessorStatus($this);
        }
        return false;
    }
}
