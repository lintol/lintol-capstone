<?php

namespace Lintol\Capstone\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Alsofronie\Uuid\UuidModelTrait;
use App\User;
use Cron\CronExpression;

class DataResource extends Model
{
    use UuidModelTrait;

    protected $fillable = [
         'filename',
         'url',
         'name',
         'filetype',
         'status',
         'remote_id',
         'user_id',
         'package_id',
         'ckan_instance_id',
         'source',
         'user',
         'archived',
         'reportid',
         'content',
         'settings',
         'size',
         'locale',
         'organization'
    ];

    public $present = true;

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public $casts = [
        'remote_metadata' => 'json',
        'settings' => 'json',
        'source' => 'json',
        'last_scheduled_run' => 'datetime:Y-m-d H:i:s',
        'updated_externally_at' => 'datetime:Y-m-d H:i:s'
    ];

    public function run()
    {
        return $this->hasMany(ValidationRun::class);
    }

    public function statusChanges()
    {
        return $this->hasMany(DataResourceStatusChange::class);
    }

    public function setStatus($status, $detail=null)
    {
        if ($this->id) {
            $this->status = $status;
            $change = new DataResourceStatusChange;
            $change->data_resource_id = $this->id;
            $change->new_status = $status;
            $change->detail = $detail;
            $change->save();
        }
    }

    public function resourceable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getPackageNameAttribute()
    {
        return $this->package ? $this->package->name : '';
    }

    public function package()
    {
        return $this->belongsTo(DataPackage::class, 'package_id');
    }

    public function summaryByStatus($createdSince=null)
    {
        $query = DB::table('data_resources');

        if ($createdSince) {
            $query = $query->where('created_at', '>', $createdSince);
        }

        return $query
            ->select('status', DB::raw('count(*) as total'))
            ->groupBy('status')
            ->pluck('total', 'status')
            ->all();
    }

    public function getLastRunByInitiatedAt()
    {
        return $this->run()
                    ->whereNotNull('initiated_at')
                    ->orderBy('initiated_at', 'desc')
                    ->first();
    }

    public function runCountsByCompletionStatus()
    {
        return $this->run()
            ->selectRaw('completion_status, count(completion_status) as status_count')
            ->groupBy('completion_status')
            ->havingRaw('count(completion_status) > 0')
            ->get()
            ->map(function ($result) {
                $result['completion_status_string'] = ValidationRun::COMPLETION_STATUS_CODES[$result['completion_status'] ?: 0];
                return $result;
            })
            ->pluck('status_count', 'completion_status_string');
    }

    public function dueToBeChecked()
    {
        if (!$this->run_schedule) {
            return false;
        }

        $lastChecked = $this->last_scheduled_run;

        if ($lastChecked) {
            $lastChecked->setSeconds(59);
        }

        $runSchedule = CronExpression::factory($this->run_schedule);
        if (!$lastChecked || $lastChecked < $runSchedule->getPreviousRunDate()) {
            return true;
        }
        return false;
    }
}
