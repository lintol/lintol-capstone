<?php

namespace Lintol\Capstone\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use App\User;

class ProcessorConfiguration extends Model
{
    use UuidModelTrait;

    protected $casts = [
        'definition' => 'json',
        'configuration' => 'json',
        'user_configuration_storage' => 'json',
        'artifacts_requested' => 'json',
        'rules' => 'json'
    ];

    protected $fillable = [
         'user_configuration_storage',
         'artifacts_requested',
         'processor_id',
         'configuration',
         'definition',
         'rules'
    ];

    public function filters()
    {
        return [];
    }

    public function rules()
    {
        return [];
    }

    public function processor()
    {
        return $this->belongsTo(Processor::class);
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function updateDefinition()
    {
        if ($this->processor) {
            $this->rules = $this->processor->rules;
            $this->definition = $this->processor->definition;
        }
    }

    public function buildDefinition()
    {
        $this->configuration = array_merge(
            json_decode(json_encode($this->processor->configuration_defaults), true),
            ['artifactsRequested' => $this->artifacts_requested],
            json_decode(json_encode($this->user_configuration_storage), true)
        );

        $supplementary = [];
        $supplementaryToFind = [];
        foreach ($this->configuration as $key => $value) {
          if (is_string($value) && strlen($value) > 3 && substr($value, 0, 3) == '$->') {
            $supplementaryToFind[] = substr($value, 3);
          }
        }

        if (array_key_exists('dependencies', $this->definition)) {
          foreach ($this->definition['dependencies'] as $value) {
            if (strlen($value) > 3 && substr($value, 0, 3) == '$->') {
              $supplementaryToFind[] = substr($value, 3);
            }
          }
        }


        foreach ($supplementaryToFind as $value) {
          if ($this->processor) {
            if (array_key_exists($value, $this->processor->supplementary_links)) {
                $location = basename($this->processor->supplementary_links[$value]);
                $supplementary[$value] = [
                    'location' => $location,
                    'source' => $this->processor->supplementary_links[$value]
                ];
            } else {
              $supplementary[$value] = [
                  'source' => 'error://link-not-found',
                  'location' => 'error://link-not-found'
              ];
            }
          } else {
            $supplementary[$value] = [
                  'source' => 'error://processor-details-missing',
                  'location' => 'error://processor-details-missing'
              ];
          }
        }

        if (empty($supplementary)) {
            $supplementary = (object) [];
        }

        $module = null;
        if ($this->processor && $this->processor->module) {
          $module = $this->processor->module;
        }

        $tag = null;
        if ($this->processor && $this->processor->unique_tag) {
          $tag = $this->processor->unique_tag;
        }

        return [
            'configuration' => $this->configuration,
            'definition' => $this->definition,
            'supplementary' => $supplementary,
            'tag' => $tag,
            'module' => $module
        ];
    }
}
