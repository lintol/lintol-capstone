<?php

namespace Lintol\Capstone\Models;

use Illuminate\Database\Eloquent\Model;
use Lintol\Capstone\Events\DataResourceStatusChangedEvent;

class DataResourceStatusChange extends Model
{
    public $casts = [
        'detail' => 'json'
    ];

    public function dataResource() {
        return $this->belongsTo(DataResource::class, 'data_resource_id');
    }

    protected $dispatchesEvents = [
        'saved' => DataResourceStatusChangedEvent::class
    ];
}
