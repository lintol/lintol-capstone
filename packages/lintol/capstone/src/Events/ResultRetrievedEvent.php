<?php

namespace Lintol\Capstone\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Transformers\ReportTransformer;

class ResultRetrievedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $validationId;

    public $reportJson = null;

    /**
     * Create a new event instance.
     *
     * @param $validationId ID of the validation that has been returned
     * @return void
     */
    public function __construct($validationId)
    {
        $this->validationId = $validationId;

        $validation = ValidationRun::find($validationId);

        if ($validation) {
            $report = $validation->report;
            if ($report) {
                if ($report->owner) {
                    $report->owner->retrieve();
                }

                $transformer = app()->make(ReportTransformer::class);
                $this->reportJson = fractal()
                    ->item($report, $transformer, 'reports')
                    ->toArray();
            }
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\PrivateChannel
     */
    public function broadcastOn()
    {
        $channels = [
            new PrivateChannel('run.' . $this->validationId)
        ];

        $validation = ValidationRun::find($this->validationId);

        if ($validation && $validation->dataResource) {
            $dataResource = $validation->dataResource;
            $channels[] = new PrivateChannel('data-resource.' . $dataResource->id);

            if ($dataResource->remote_id) {
                $channels[] = new PrivateChannel('data-resource.remote-' . $dataResource->remote_id);
            }
        }

        return $channels;
    }
}
