<?php

namespace Lintol\Capstone\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DataResourceStatusChangedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $dataResourceId;

    public $dataResourceNewStatus;

    public $dataResourceRemoteId;

    public $dataResourceStatusChangeId;

    /**
     * Create a new event instance.
     *
     * @param $validationId ID of the validation that has been returned
     * @return void
     */
    public function __construct($dataResourceStatusChange)
    {
        $this->dataResourceId = $dataResourceStatusChange->data_resource_id;
        $this->dataResourceNewStatus = $dataResourceStatusChange->new_status;
        $this->dataResourceRemoteId = $dataResourceStatusChange->dataResource->remote_id;
        $this->dataResourceStatusChangeId = $dataResourceStatusChange->id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\PrivateChannel
     */
    public function broadcastOn()
    {
        $channels = [new PrivateChannel('data-resource.' . $this->dataResourceId)];

        if ($this->dataResourceRemoteId) {
            $channels[] = new PrivateChannel('data-resource.remote-' . $this->dataResourceRemoteId);
        }

        return $channels;
    }
}
