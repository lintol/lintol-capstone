<?php

namespace Lintol\Capstone\Console\Commands;

use Log;
use App;
use File;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Models\Processor;
use Lintol\Capstone\Models\ProcessorConfiguration;
use Lintol\Capstone\Models\Profile;
use Lintol\Capstone\Models\DataResource;
use Illuminate\Console\Command;
use Lintol\Capstone\Jobs\StatusRetrieveJob;

class StatusRetrieveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ltl:status-retrieve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve processor statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        StatusRetrieveJob::dispatch();
    }
}
