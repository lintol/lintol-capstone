<?php

namespace Lintol\Capstone\Console\Commands;

use Illuminate\Console\Command;
use Lintol\Capstone\Jobs\CheckScheduledDataResourcesJob;

class CheckScheduledDataResourcesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ltl:check-scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and run any scheduled datasets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CheckScheduledDataResourcesJob::dispatch()
            ->onConnection('sync');
    }
}
