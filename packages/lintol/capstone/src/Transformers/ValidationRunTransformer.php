<?php

namespace Lintol\Capstone\Transformers;

use League\Fractal;
use Lintol\Capstone\Models\ValidationRun;
use Lintol\Capstone\Models\DataResource;
use Lintol\Capstone\Models\Profile;

class ValidationRunTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
    ];

    protected $availableIncludes = [
        'profile',
        'dataResource'
    ];

    public function transform(ValidationRun $data)
    {
        return [
            'id' => $data->id,
            'createdAt' => $data->created_at,
            'updatedAt' => $data->updated_at,
            'requestedAt' => $data->requested_at,
            'initiatedAt' => $data->initiated_at,
            'completedAt' => $data->completed_at,
            'settings' => $data->settings,
            'doorstepServerId' => $data->doorstep_server_id,
            'doorstepSessionId' => $data->doorstep_session_id,
            'doorstepDefinition' => $data->doorstep_definition
        ];
    }

    public function includeDataResource(ValidationRun $run)
    {
        if ($run->data_resource) {
            return $this->item(
                $run->data_resource,
                new DataResourceTransformer,
                'resources'
            );
        }

        return null;
    }

    public function includeProfile(ValidationRun $run)
    {
        if ($run->profile) {
            return $this->item(
                $run->profile,
                new ProfileTransformer,
                'profiles'
            );
        }

        return null;
    }
}
