<?php

namespace Lintol\Capstone\Transformers;

use League\Fractal;
use Mimey\MimeTypes;
use Lintol\Capstone\Models\DataResource;
use App\Transformers\UserTransformer;

class DataResourceTransformer extends Transformer
{
    protected $availableIncludes = [
        'package',
        'lastRun'
    ];

    protected $defaultIncludes = [
        'user'
    ];

    protected $options = [
        'withRemoteMetadata' => false
    ];

    public function transform(DataResource $data)
    {
        $mimes = app()->make(MimeTypes::class);

        if ($data->package) {
            $packageName = $data->package->name;
        } else {
            $packageName = '';
        }

        $runCount = $data->run->count();
        $runByStatusCount = $data->runCountsByCompletionStatus();
        if (!count($runByStatusCount)) {
            $runByStatusCount = null;
        }

        if ($data->status) {
            $lastStatusChange = $data->statusChanges()
                                     ->orderBy('id', 'desc')
                                     ->first();
        } else {
            $lastStatusChange = null;
        }

        // TODO: move this to ingest and store it
        if ($data->filetype) {
            if (strpos($data->filetype, '/')) {
                $mimetype = $data->filetype;
                $filetype = $mimes->getExtension($data->filetype);
            } else {
                $filetype = $data->filetype;
                $mimetype = $mimes->getMimeType($data->filetype);
            }
        } else {
            $mimetype = null;
            $filetype = null;
        }

        $lastRun = $data->getLastRunByInitiatedAt();
        $response = [
            'id' => ($data->id || !$data->remote_id) ? $data->id : 'remote-' . $data->remote_id,
            'filename' => $data->filename,
            'url' => $data->url,
            'filetype' => $filetype,
            'mimetype' => $mimetype,
            'packageName' => $packageName,
            'packageRemoteId' => $data->package ? $data->package->remote_id : null,
            'packageId' => $data->package_id,
            'organization' => $data->organization,
            'status' => $data->status,
            'lastStatusChange' => $lastStatusChange,
            'source' => $data->source,
            'created_at' => null,
            'importedAt' => $data->created_at,
            'archived' => $data->archived,
            'providerId' => $data->resourceable ? $data->resourceable->id : null,
            'providerType' => $data->resourceable ? $data->resourceable->driver : null,
            'providerServer' => $data->resourceable ? $data->resourceable->uri : null,
            'providerResourceURI' => $data->resourceable ? $data->resourceable->uriFor($data) : null,
            'providerPackageURI' => $data->resourceable ? $data->resourceable->uriFor($data, true) : null,
            'updatedExternallyAt' => $data->updated_externally_at,
            'lastScheduledRun' => $data->last_scheduled_run,
            'runSchedule' => $data->run_schedule,
            'size' => $data->size,
            'locale' => $data->locale,
            'lastRunStatus' => $lastRun ? $lastRun->statusCode : null,
            'lastRunCompletionDate' => $lastRun ? $lastRun->completed_at : null,
            'runs' => $runCount,
            'runsByCompletionStatusCount' => $runByStatusCount,
            'remote_id' => $data->remote_id,
        ];

        if ($this->options['withRemoteMetadata']) {
            $response['remoteMetadata'] = $data->remote_metadata;
        }

        if ($data->remote_metadata && isset($data->remote_metadata['created'])) {
            $response['created_at'] = $data->remote_metadata['created'];
        }

        return $response;
    }

    public function includeUser(DataResource $data)
    {
        if ($data->user) {
            return $this->item(
                $data->user,
                new UserTransformer,
                'users'
            );
        }

        return null;
    }

    public function includePackage(DataResource $data)
    {
        if ($data->package) {
            return $this->item(
                $data->package,
                new DataPackageTransformer,
                'packages'
            );
        }

        return null;
    }

    public function includeLastRun(DataResource $data)
    {
        $run = $data->getLastRunByInitiatedAt();
        if ($run) {
            return $this->item(
                $run,
                new ValidationRunTransformer,
                'runs'
            );
        }

        return null;
    }
}
