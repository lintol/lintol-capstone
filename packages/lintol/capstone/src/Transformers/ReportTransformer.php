<?php

namespace Lintol\Capstone\Transformers;

use App\Transformers\UserTransformer;
use League\Fractal;
use Lintol\Capstone\Models\Report;

class ReportTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
        'user'
    ];

    protected $availableIncludes = [
        'run',
        'dataResource',
        'dataPackage'
    ];

    public function transform(Report $report)
    {
        return [
            'id' => $report->id,
            'name' => $report->name,
            'profile' => $report->run && $report->run->profile ? $report->run->profile->name : $report->profile,
            'errors' => $report->errors,
            'warnings' => $report->warnings,
            'passes' => $report->passes,
            'qualityScore' => $report->quality_score,
            'content' => $report->content,
            'createdAt' => $report->created_at,
            'profileId' => $report->getProfileId(),
            'dataResourceId' => $report->getDataResourceId()
        ];
    }

    public function includeUser(Report $report)
    {
        if ($report->owner) {
            return $this->item(
                $report->owner,
                new UserTransformer,
                'users'
            );
        }

        return null;
    }

    public function includeDataResource(Report $report)
    {
        if ($report->data_resource) {
            $dataResourceTransformer = new DataResourceTransformer;
            $dataResourceTransformer->setOption('withRemoteMetadata', true);
            return $this->item(
                $report->data_resource,
                $dataResourceTransformer,
                'resources'
            );
        }

        return null;
    }

    public function includeDataPackage(Report $report)
    {
        if ($report->data_resource) {
            return $this->item(
                $report->data_resource->package,
                new DataPackageTransformer,
                'packages'
            );
        }

        return null;
    }

    public function includeRun(Report $report)
    {
        if ($report->run) {
            return $this->item(
                $report->run,
                new ValidationRunTransformer,
                'runs'
            );
        }

        return null;
    }
}
