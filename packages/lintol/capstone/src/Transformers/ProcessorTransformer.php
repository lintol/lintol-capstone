<?php

namespace Lintol\Capstone\Transformers;

use League\Fractal;
use Lintol\Capstone\Models\Processor;

class ProcessorTransformer extends Transformer
{
    protected $options = [
        'withDefinition' => false
    ];

    public function transform(Processor $processor)
    {
        // TODO: Once processors are upgraded, this hard-coded default should be removed
        $reportCsv = [
            'name' => 'Report (CSV)',
            'description' => 'CSV',
            'mime' => 'text/csv'
        ];

        $properties = [
            'id' => $processor->id,
            'name' => $processor->name,
            'status' => $processor->status,
            'description' => $processor->description,
            'creatorId' => $processor->creator_id,
            'uniqueTag' => $processor->unique_tag,
            'module' => $processor->module,
            'created_at' => $processor->created_at,
            'updated_at' => $processor->updated_at,
            'status' => $processor->status,
            'artifactsAvailable' => $processor->artifacts_available !== null ? $processor->artifacts_available : ['report:csv' => $reportCsv],
            'configurationCount' => $processor->configurations()->count(),
            'configurationOptions' => !empty($processor->configuration_options) ? $processor->configuration_options : new \stdClass,
            'configurationDefaults' => !empty($processor->configuration_defaults) ? $processor->configuration_defaults : new \stdClass,
        ];

        if (array_key_exists('withDefinition', $this->options) && $this->options['withDefinition']) {
            $properties['definition'] = json_encode($processor->definition);
        }

        if (array_key_exists('withSupplementary', $this->options) && $this->options['withSupplementary']) {
            $properties['supplementaryLinks'] = json_encode($processor->supplementary_links);
        }

        return $properties;
    }
}
