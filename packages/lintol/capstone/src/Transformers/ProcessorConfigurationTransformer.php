<?php

namespace Lintol\Capstone\Transformers;

use League\Fractal;
use Lintol\Capstone\Models\Processor;
use Lintol\Capstone\Models\ProcessorConfiguration;

class ProcessorConfigurationTransformer extends Transformer
{
    protected $defaultIncludes = [
        'processor'
    ];

    public static function inputMapping()
    {
        return [
            'artifactsRequested' => function ($artifactsRequested) {
                return ['artifacts_requested' => $artifactsRequested];
            },
            'userConfigurationStorage' => function ($userConfigurationStorage) {
                if (is_string($userConfigurationStorage) === true) {
                    $userConfigurationStorage = json_decode($userConfigurationStorage);
                }
                if (is_string($userConfigurationStorage) === true) {
                    $userConfigurationStorage = json_decode($userConfigurationStorage);
                }
                return ['user_configuration_storage' => $userConfigurationStorage];
            },
            'processor' => function ($processor) {
                return ['processor_id' => $processor['id']];
            }
        ];
    }

    public function transform(ProcessorConfiguration $configuration)
    {
        $userConfigurationStorage = $configuration->user_configuration_storage;

        if (empty($userConfigurationStorage)) {
            $userConfigurationStorage = new \stdClass;
        }

        return [
            'id' => $configuration->id,
            'artifactsRequested' => $configuration->artifacts_requested ?? new \stdClass,
            'userConfigurationStorage' => $userConfigurationStorage
        ];
    }

    public function includeProcessor(ProcessorConfiguration $configuration)
    {
        return $this->item($configuration->processor, new ProcessorTransformer, 'processors');
    }
}
