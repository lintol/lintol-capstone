<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArtifactsAvailableToProcessors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processors', function (Blueprint $table) {
            $table->json('artifacts_available')->nullable();
        });

        Schema::table('processor_configurations', function (Blueprint $table) {
            $table->json('artifacts_requested')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processors', function (Blueprint $table) {
            $table->dropColumn('artifacts_available');
        });

        Schema::table('processor_configurations', function (Blueprint $table) {
            $table->dropColumn('artifacts_requested');
        });
    }
}
