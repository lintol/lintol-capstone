<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckingScheduleToDataResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_resources', function (Blueprint $table) {
            $table->datetime('last_scheduled_run')->nullable();
            $table->datetime('updated_externally_at')->nullable();
            $table->string('run_schedule', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_resources', function (Blueprint $table) {
            $table->dropColumn('last_scheduled_run');
            $table->dropColumn('updated_externally_at');
            $table->dropColumn('run_schedule');
        });
    }
}
